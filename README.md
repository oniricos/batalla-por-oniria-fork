# Battle For Oniria

Un pequeño proyecto open source para aprender a hacer un juego colaborativo. Inspirado por Battle for Wesnoth. Basado en el mundo de Oniria World.

https://oniria.world/comunidad_/

https://oniria-world.fandom.com/es/wiki/Wiki_Oniria_World

## Instrucciones para colaborar

1.- Clona este proyecto

2.- Crea una rama nueva

3.- Dentro de Unity, crea una carpeta para tu facción

4.- No modifiques ninguna escena que no hayas hecho tú y trabaja siempre dentro de tu carpeta

5.- Actualiza y sube tu rama

6.- Espera a que un admin la incorpore


## Ayuda al desarrollo

### Paleta de colores
La paleta más usada en Oniria World es:

Negro  - #282628

Blanco - #e4e2e4

Violeta muy oscuro - #2c0f33

Violeta oscuro - #7463a9

Violeta claro  - #cfaad7

Amarillo - #e9e0ba
