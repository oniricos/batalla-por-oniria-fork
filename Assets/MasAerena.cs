using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasAerena : MonoBehaviour
{
    [SerializeField] UIManager[] uIManagers;
    [SerializeField] Recursos[] recursos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            
            for (int i = 0; i < recursos.Length; i++)
            {
                recursos[i].aumentarAerena(20);
                uIManagers[i].ContadorAErena();
            }
        }
    }
}
