using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{
    public static event Action<Vida> OnDiying;
    [SerializeField] int vida;
   public void QuitarVida()
    {
         --vida;
        if (vida <= 0)
        {
            Destroy(gameObject);
            OnDiying?.Invoke(this);
        }
    }
}
