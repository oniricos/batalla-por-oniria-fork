using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackMinion : MonoBehaviour
{
    [SerializeField] MySide Lado;
    [SerializeField] NavMeshAgent Nose;
    [SerializeField] List<GameObject> Enemigos = new List<GameObject>();
    [SerializeField] float DistanciaInicial = 2f;
    [SerializeField] Animator animator;
    [SerializeField] GameObject proyectil;
    bool attacking = false;
    private void Awake()
    {
        Lado = GetComponent<Lado>().MiLado();
    }

    private void OnTriggerEnter(Collider collision)
    {
        switch (Lado) {

            case MySide.TripleEstrella: 
                if (collision.gameObject.layer == 8 && collision.gameObject.tag == "UnidadOnirica" )
                  {
                  GetComponent<Vida>().QuitarVida();
                  break;
                 }
                break;
            case MySide.Oniricos:
                if (collision.gameObject.layer == 8 && collision.gameObject.tag == "UnidadTripleEstrella")
                {
                    GetComponent<Vida>().QuitarVida();
                    break;
                }
                break;
        }
}
    // Update is called once per frame
    void Update()
    {
        if(Enemigos.Count > 0) { 
            if (Vector3.Distance(transform.position,Enemigos[0].transform.position) >= DistanciaInicial )
             {
            
            Vector3 posicionEnemigo = new Vector3(Enemigos[0].transform.position.z - DistanciaInicial, Enemigos[0].transform.position.y, Enemigos[0].transform.position.x - DistanciaInicial);
            Nose.SetDestination(posicionEnemigo);
                
              }
            else
             {
                transform.LookAt(Enemigos[0].transform);
                if (!attacking) {
                    
                    StartCoroutine(AttackEnemyMinion());
                    
                }
                
                animator.SetBool("Atacando", true);
                animator.SetBool("Caminando", false);
                Nose.isStopped = true;
             }
        }
        else
        {

            StopCoroutine(AttackEnemyMinion());
            animator.SetBool("Atacando", false);
            Nose.isStopped = false;
           
        }
    }
    
   IEnumerator AttackEnemyMinion()
    {
        while (Enemigos.Count > 0) { 
            attacking = true;

            if(Enemigos[0] != null) 
            { 
             
             GameObject proyecti= Instantiate(proyectil,transform.position,transform.rotation);
             proyecti.tag = gameObject.tag;
       
             proyecti.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * 100);
            }
            yield return new WaitForSeconds(1f);
        attacking = false;
        }
    }
    public void AddEnemigo(GameObject a)
    {
        Enemigos.Add(a);
    }
   public void EnemyDie(GameObject a)
    {
        Enemigos.Remove(a);
    }
}
