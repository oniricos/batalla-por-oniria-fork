using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementProyectil : MonoBehaviour
{
    Vector3 Objetivo;
    public float tiempoRespuestaIA;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Objetivo !=null) {
            Vector3 posicion = new Vector3(Objetivo.x, transform.position.y, Objetivo.z);

            posicion.z = Mathf.Lerp(transform.position.z, posicion.z, tiempoRespuestaIA);
            posicion.x = Mathf.Lerp(transform.position.x, posicion.x, tiempoRespuestaIA);
            transform.position = posicion;
        }
        else
        {
            Destroy(gameObject);
        }
        

        
    }
   

    private void OnTriggerEnter(Collider collision)
    {
        if(gameObject.tag != collision.gameObject.tag) Destroy(gameObject);
    }
    public void AsignarObjetivo(Vector3 a)
    {
        Objetivo = a;
    }
}
