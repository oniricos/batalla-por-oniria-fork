using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarEnemigo : MonoBehaviour
{
    [SerializeField] MySide Lado;
    [SerializeField] AttackMinion attack;
    private void Awake()
    {
        Lado = GetComponentInParent<Lado>().MiLado();
    }

    private void OnEnable()
    {
        Vida.OnDiying += minionDead; 
    }
    private void OnDisable()
    {
        Vida.OnDiying -= minionDead;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(Lado == MySide.TripleEstrella && other.gameObject.tag == "UnidadOnirica")
        {
            attack.AddEnemigo(other.gameObject);
            
        }
        if (Lado == MySide.Oniricos && other.gameObject.tag == "UnidadTripleEstrella")
        {
            attack.AddEnemigo(other.gameObject);
           
        }
    }
    void minionDead(Vida vida)
    {
        attack.EnemyDie(vida.gameObject);
    }
    private void OnTriggerExit(Collider other)
    {
        if (Lado == MySide.TripleEstrella && other.gameObject.tag == "UnidadOnirica")
        {
            attack.EnemyDie(other.gameObject);

        }
        if (Lado == MySide.Oniricos && other.gameObject.tag == "UnidadTripleEstrella")
        {
            attack.EnemyDie(other.gameObject);

        }
    }
}
