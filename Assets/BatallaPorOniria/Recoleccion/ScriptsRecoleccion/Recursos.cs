using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recursos : MonoBehaviour
{
    [SerializeField] int aerena = 0;

    public void aumentarAerena(int a)
    {
        aerena = aerena + a;
    }
    public void DisminuirAerena(int a)
    {
        aerena = aerena - a;
    }
    public int AerenaActual()
    {
       return aerena;
    }
}
