using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detectar : MonoBehaviour
{
    [SerializeField] List<GameObject> minionsEstrella = new List<GameObject>();
    [SerializeField] List<GameObject> minionsOnirico = new List<GameObject>();
    [SerializeField] TamanoRecurso tamano = TamanoRecurso.Grande;
    [SerializeField] float tiempo = 1;
    [SerializeField] float tiempoAerena = 1;
    [SerializeField] int cantidadAerena = 3;
    [SerializeField] Recursos recursosEstrella;
    [SerializeField] Recursos recursosOniricos;
    [SerializeField] Mesh[] Plantas;
    [SerializeField] MeshFilter Planta;
    [SerializeField] UIManager uiManager;
    private void Awake()
    {
        uiManager = FindObjectOfType<UIManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        LadoAlQueDarAerena(other.GetComponent<Lado>().equipo, other.gameObject);
        other.GetComponentInChildren<Canvas>().enabled = true;
           
        
    }
    private void OnTriggerStay(Collider other)
    {
        if (temporizador() <= 0 && cantidadAerena >= 1)
        {

            if (minionsEstrella.Count > 0) { uiManager.ContadorAErena(); recursosEstrella.aumentarAerena(cantidadAerena); }

            if (minionsOnirico.Count > 0) { uiManager.ContadorAErena(); recursosOniricos.aumentarAerena(cantidadAerena); }
            tiempo = 1;
        }
        if (temporizadorCambioDeRecurso() <= 0 && cantidadAerena >= 1)
        {
            cambioDeRecurso();
            
        }
        if (cantidadAerena == 0)
        {
            other.GetComponentInChildren<Canvas>().enabled = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        QuitarMinionsyNoDarAerena(other.GetComponent<Lado>().equipo, other.gameObject);
        other.GetComponentInChildren<Canvas>().enabled = false;   
    }

    void cambioDeRecurso()
    {
        switch (tamano){
            case TamanoRecurso.Grande:
                tamano = TamanoRecurso.Mediano;
                tiempoAerena = 30;
                cantidadAerena = 3;
                break;
            case TamanoRecurso.Mediano:
                tamano = TamanoRecurso.Pequeno;
                Planta.mesh = Plantas[0];
                tiempoAerena = 20;
                cantidadAerena = 2;
                break;
            case TamanoRecurso.Pequeno:
                tamano = TamanoRecurso.Nada;
                Planta.mesh = Plantas[1];
                tiempoAerena = 10;
                cantidadAerena = 1;
                break;
            case TamanoRecurso.Nada:
                Planta.mesh = Plantas[2];
                cantidadAerena =0;
                break;
        }
    }
    float temporizadorCambioDeRecurso()
    {
        if (cantidadAerena >= 1) return tiempoAerena = tiempoAerena - Time.deltaTime;

        return 0;
    }
    float temporizador()
    {

        if (cantidadAerena >= 1) return tiempo = tiempo - Time.deltaTime;
        return 0;
    }

    void LadoAlQueDarAerena(MySide a, GameObject b)
    {
        switch (a)
        {
            case MySide.TripleEstrella:
               recursosEstrella = GameObject.FindGameObjectWithTag("CasaTripleEstrella").GetComponent<Recursos>();
                minionsEstrella.Add(b);
                break;

            case MySide.Oniricos:
                recursosOniricos = GameObject.FindGameObjectWithTag("CasaOniricos").GetComponent<Recursos>();
                minionsOnirico.Add(b);
                break;

        }
    }
    void QuitarMinionsyNoDarAerena(MySide a, GameObject b)
    {
        switch (a)
        {
            case MySide.TripleEstrella:
                if (minionsEstrella.Count <= 0) recursosEstrella = null;
                minionsEstrella.Remove(b);
                break;

            case MySide.Oniricos:
                if(minionsOnirico.Count <= 0) recursosOniricos = null;
                minionsOnirico.Remove(b);
                break;

        }
    }
}
