using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource SonidoV;

    public AudioClip Prueba;
    
    public void Sonido()
    {
        SonidoV.clip = Prueba;
        SonidoV.Play();
    }

}
