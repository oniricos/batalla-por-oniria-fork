using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject panelEdificio;
    public GameObject botonPequeno;
    public GameObject descripcionBotonPequeno;
    public GameObject botonGrande;
    public GameManager gameManager;
    public GameObject botonCrearUnidad;
    [SerializeField] Recursos aerena;
    [SerializeField] Text aerenas;

    public void ActivarControlesBase(bool activar=true)
    {
        botonCrearUnidad.SetActive(activar);
    }
    public void ContadorAErena()
    {
        //gameObject.metodoPertinente.ToString();
        aerenas.text = aerena.AerenaActual().ToString();
    }
    public void ContadorTiempo()
    {
        gameManager.ContadorTiempo();
    }
    public void AccesoEdificio()
    {
        //Despliegue de panel de informaci�n al pinchar en el edificio
        //if onmouse gameobject de la clase del edificio 
        panelEdificio.SetActive(true);

    }
    public void CrearUnidad()
    {
        botonPequeno.SetActive(true);
        botonGrande.SetActive(true);
    }
    public void PulsarBotonPeque�o()
    {
        //llamada a otro metodo de otra clase encargada de crear unidad peque�a
    }
    public void PulsarBotonGrande()
    {
        //llamada a otro metodo de otra clase encargada de crear unidad grande
    }

   
}
