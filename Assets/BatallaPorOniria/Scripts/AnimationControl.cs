using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimationControl : MonoBehaviour
{
    [SerializeField] NavMeshAgent jugador;
    [SerializeField] Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(jugador.hasPath == true)
        {
            animator.SetBool("Caminando", true);
        }else
        {
            animator.SetBool("Caminando", false);
        }
    }
}
