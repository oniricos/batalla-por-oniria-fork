using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    public UIManager uiManager;
    public GameObject prefabUnidadPeque�a;
    public GameObject prefabUnidadGrande;
    int costesPeque�o = 20;
    int costesGrande= 40;
    [SerializeField] Transform Salida;
    [SerializeField] Recursos aerena;
    



    private void Awake()
    {
        CrearUnidadPeque�a();
    }

    public void OnMouseUp()
    {
        uiManager.AccesoEdificio();
    }

    public void CrearUnidadPeque�a()
    {
        if (aerena.AerenaActual() >= costesPeque�o && aerena.AerenaActual() - costesPeque�o >= 0)
        {
            aerena.DisminuirAerena(costesPeque�o);
            uiManager.ContadorAErena();
            Vector3 random = new Vector3(Salida.position.x + Random.Range(0f, 2f), 0, Salida.position.z + Random.Range(0f, 2f));

            Instantiate(prefabUnidadPeque�a, random, Quaternion.identity);
            prefabUnidadPeque�a.transform.Translate(2, 2, 2);
        }
       


    }
    public void CrearUnidadGrande()
    {
        if (aerena.AerenaActual() >= costesGrande && aerena.AerenaActual() - costesGrande >= 0)
        {
            aerena.DisminuirAerena(costesGrande);
            uiManager.ContadorAErena();
            Vector3 random = new Vector3(Salida.position.x + Random.Range(0f, 2f), 0, Salida.position.z + Random.Range(0f, 2f));

            Instantiate(prefabUnidadGrande, random, Quaternion.identity);
            prefabUnidadGrande.transform.Translate(2, 2, 2);
        }
    }

    public int CostePeque�o()
    {
        return costesPeque�o;
    }
    public int CosteGrande()
    {
        return costesGrande;
    }
}
