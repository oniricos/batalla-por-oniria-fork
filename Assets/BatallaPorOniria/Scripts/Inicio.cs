using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Inicio : MonoBehaviour
{
 
    public void SelectEquipo1()
    {
        SceneManager.LoadScene(1);

    }
    public void SelectEquipo2()
    {
        SceneManager.LoadScene(2);
    }

    //Exit
    public void Salida()
    {
        Application.Quit();
    }
}
