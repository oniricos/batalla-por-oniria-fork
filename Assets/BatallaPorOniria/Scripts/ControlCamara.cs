using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public float sensibilidad=4;
    public bool usarPuntero = false;
    public float margen = 0.05f;
    [SerializeField] Transform Limite;
    
    void LateUpdate()
    {
        float x = Input.GetAxis("Horizontal") * sensibilidad;
        float z = Input.GetAxis("Vertical"  ) * sensibilidad;

        float ratonx=0;
        float ratonz=0;
        if (Input.mousePosition.x < Screen.width * margen) ratonx = -1 * sensibilidad;
        if (Input.mousePosition.x > (Screen.width-Screen.width * margen)) ratonx = +1 * sensibilidad;
        if (Input.mousePosition.y < Screen.height * margen) ratonz = -1 * sensibilidad;
        if (Input.mousePosition.y > (Screen.height - Screen.height * margen)) ratonz = +1 * sensibilidad;

        Vector3 origen = transform.position;
        Vector3 destino = origen + new Vector3(x, 0, z);
        if(usarPuntero) destino += new Vector3(ratonx, 0, ratonz);
        destino.x = Mathf.Clamp(destino.x ,- Limite.position.x, Limite.position.x);
        destino.z = Mathf.Clamp(destino.z, -Limite.position.z, Limite.position.z);

        transform.position = Vector3.Lerp(origen, destino, Time.deltaTime);
    }
}
