using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text TextoTiempo;
   
    float tiempo = 0.0f;

    int minutos = 0;
    int segundos = 0;
    int horas = 0;
    //public Unidad unidadSeleccionada;

    void Update()
    {
        ClickRaycast();
        ContadorTiempo();
    }

    void ClickRaycast()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;

            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(rayo.origin, rayo.direction * 100, Color.green);
            bool choque = Physics.Raycast(rayo, out hit, 100);
            if (choque)
            {
                /*if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Escenario")
                {
                    unidadSeleccionada.Ir(hit.point);
                }*/
            }
        }

        /*if(Input.GetMouseButtonDown(0)){
            Unidad[] unidades = FindObjectsOfType<Unidad>();
            foreach(Unidad u in unidades)
            {
                u.Desactivar();
            }
        }*/
    }
    /*public void SeleccionarUnidad(Unidad u)
    {
        unidadSeleccionada = u;
    }
    */
    public void ContadorTiempo()
    {
        tiempo += Time.deltaTime;

        minutos = (int)tiempo / 60;
        segundos = (int)tiempo % 60;

        TextoTiempo.text = horas.ToString("00") + ":" + minutos.ToString("00") + ":" + segundos.ToString("00");
    }
}
